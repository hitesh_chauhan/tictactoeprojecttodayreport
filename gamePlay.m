//
//  gamePlay.m
//  tictactoeproject
//
//  Created by Click Labs108 on 10/15/15.
//  Copyright © 2015 tashanpunjabi. All rights reserved.
//

#import "gamePlay.h"
#import "PlayersView.h"
#import "ViewController.h"

@implementation gamePlay

{
    PlayersView*myplayerview;
    UIView*lineView1;
    UIView*lineView2;
    UIView*lineView3;
    UIView*lineView4;
    UIView*lineView5;
    UIView*lineView6;
    UILabel*firstscore;
    UILabel*secondScore;
    ViewController*mainController;
    UIButton*firstboxbutton;
    UIButton*secondboxbutton;
    UIButton*thirdboxbutton;
    UIButton*forthboxbutton;
    UIButton*fifthboxbutton;
    UIButton*sixthboxbutton;
    UIButton*seventhboxbutton;
    UIButton*eightboxbutton;
    UIButton*ninethboxbutton;
    UIView * lineView;
    
    int p;
    
    int winningCombinations;
    int counter;
}

-(void) playersnames{
    
    
    self.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"6799151-ios-background.jpg"]];
  
    p=1;
    counter=0;
    _firstPlayer=[[UILabel alloc]initWithFrame:CGRectMake(10  ,48, 130, 30)];
    
    _firstPlayer.textColor=[UIColor brownColor];
    _firstPlayer.layer.borderWidth=1.5f;
    [[_firstPlayer layer] setBorderColor:[UIColor brownColor].CGColor];
    _firstPlayer.textColor=[UIColor greenColor];
   // _firstPlayer.text=myplayerview.string;
    [self addSubview:_firstPlayer];
    
    _secondPlayer=[[UILabel alloc]initWithFrame:CGRectMake(180  ,48, 130, 30)];
    _secondPlayer.textColor=[UIColor brownColor];
    [[_secondPlayer layer] setBorderColor:[UIColor brownColor].CGColor];
    _secondPlayer.textColor=[UIColor orangeColor];
    _secondPlayer.layer.borderWidth=1.5f;
  //  _secondPlayer.text=myplayerview.string1;
    [self addSubview:_secondPlayer];
    
    
    
    lineView1=[[UIView alloc]initWithFrame:CGRectMake(100, 100, 2, 300)];
    lineView1.backgroundColor=[UIColor brownColor];
    [self addSubview:lineView1];
    
    
    
    lineView2=[[UIView alloc]initWithFrame:CGRectMake(220, 100, 2, 300)];
    lineView2.backgroundColor=[UIColor brownColor];
    [self addSubview:lineView2];
    
    
    lineView3=[[UIView alloc]initWithFrame:CGRectMake(25, 200, 290, 2)];
    lineView3.backgroundColor=[UIColor brownColor];
    [self addSubview:lineView3];
    
    
    
    lineView4=[[UIView alloc]initWithFrame:CGRectMake(25, 300, 290, 2)];
    lineView4.backgroundColor=[UIColor brownColor];
    [self addSubview:lineView4];
    
    
//    lineView5=[[UIView alloc]initWithFrame:CGRectMake(25, 100, 290, 2)];
//    lineView5.backgroundColor=[UIColor brownColor];
//    [self addSubview:lineView5];
//    
//    
//    
//    lineView6=[[UIView alloc]initWithFrame:CGRectMake(25, 400, 290, 2)];
//    lineView6.backgroundColor=[UIColor brownColor];
//    [self addSubview:lineView6];
    
    
    
    firstscore=[[UILabel alloc]initWithFrame:CGRectMake(10  ,460, 60, 40)];
    
    firstscore.textColor=[UIColor brownColor];
    [[firstscore layer] setBorderColor:[UIColor blueColor].CGColor];

    firstscore.layer.borderWidth=1.5f;
    
    [self addSubview:firstscore];
    
    secondScore=[[UILabel alloc]initWithFrame:CGRectMake(250  ,460, 60, 40)];
    secondScore.textColor=[UIColor brownColor];
    [[secondScore layer] setBorderColor:[UIColor blueColor].CGColor];

    secondScore.layer.borderWidth=1.5f;
    [self addSubview:secondScore];
    
    
    UIButton*backButton=[[UIButton alloc]initWithFrame:CGRectMake(10  ,18, 80, 22)];
    backButton.backgroundColor=[UIColor lightGrayColor];
    [[backButton layer] setBorderColor:[UIColor brownColor].CGColor];
    backButton.layer.borderWidth=2.0f;
    [backButton setTitle:@"EXIT" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:backButton];
    
    
    
    UIButton*resetButton=[[UIButton alloc]initWithFrame:CGRectMake(120  ,440, 80, 75)];
    resetButton.userInteractionEnabled=TRUE;
    [resetButton setImage:[UIImage imageNamed:@"resetimage.png"]forState:UIControlStateNormal];
    [resetButton setTitle:@"   RESET" forState:UIControlStateNormal];
    resetButton.titleLabel.textAlignment=NSTextAlignmentCenter;
    [resetButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [resetButton addTarget:self action:@selector(resetButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:resetButton];
    
    
    
    
    
}


-(void)backButtonAction{
    [self.delegateFirst gamePlayBack];
    
    
}

-(void)resetButtonAction{
    
    firstboxbutton.backgroundColor=Nil;
    secondboxbutton.backgroundColor=Nil;
    thirdboxbutton.backgroundColor=Nil;
    forthboxbutton.backgroundColor=Nil;
    fifthboxbutton.backgroundColor=Nil;
    sixthboxbutton.backgroundColor=Nil;
    seventhboxbutton.backgroundColor=Nil;
    eightboxbutton.backgroundColor=Nil;
    ninethboxbutton.backgroundColor=Nil;
    [self.delegateFirst resetbuttonaction];
    
    
}



-(void)buttons{
    
    
    firstboxbutton =[[UIButton alloc]initWithFrame:CGRectMake(0  ,102, 90, 90)];
   
    //[[firstboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    firstboxbutton.tag=101;
    //firstboxbutton.layer.borderWidth=2.0f;
  
  
    [firstboxbutton addTarget:self action:@selector(firstboxButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:firstboxbutton];
    
    secondboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(0  ,205, 90, 90)];

    //[[secondboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    //secondboxbutton.layer.borderWidth=2.0f;
    secondboxbutton.tag=102;


    [secondboxbutton addTarget:self action:@selector(secondboxButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:secondboxbutton];
    
    
    thirdboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(0 ,305, 90, 90)];
  
   // [[thirdboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
   // thirdboxbutton.layer.borderWidth=2.0f;
    thirdboxbutton.tag=103;

  
    [thirdboxbutton addTarget:self action:@selector(thirdboxButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:thirdboxbutton];
    
    forthboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(120 ,102, 90, 90)];
    
   // [[forthboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    //forthboxbutton.layer.borderWidth=2.0f;
    forthboxbutton.tag=111;

    
    [forthboxbutton addTarget:self action:@selector(forthboxbuttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:forthboxbutton];
    
    
    fifthboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(120 ,202, 90, 90)];
    
    //[[fifthboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    //fifthboxbutton.layer.borderWidth=2.0f;
    fifthboxbutton.tag=112;

    
    [fifthboxbutton addTarget:self action:@selector(fifthboxbuttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:fifthboxbutton];
    
    sixthboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(120 ,302, 90, 90)];
    
   // [[sixthboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
   // sixthboxbutton.layer.borderWidth=2.0f;
    sixthboxbutton.tag=113;

    
    [sixthboxbutton addTarget:self action:@selector(sixthboxbuttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sixthboxbutton];
 
    seventhboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(230 ,102, 90, 90)];
    
    //[[seventhboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    //seventhboxbutton.layer.borderWidth=2.0f;
    
    
    [seventhboxbutton addTarget:self action:@selector(seventhboxbuttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:seventhboxbutton];
    seventhboxbutton.tag=121;

    eightboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(230 ,202, 90, 90)];
    
    //[[eightboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    //eightboxbutton.layer.borderWidth=2.0f;
    
    eightboxbutton.tag=122;

    [eightboxbutton addTarget:self action:@selector(eightboxbuttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:eightboxbutton];
    
    ninethboxbutton=[[UIButton alloc]initWithFrame:CGRectMake(230 ,302, 90, 90)];
    
    //[[ninethboxbutton layer] setBorderColor:[UIColor brownColor].CGColor];
    //ninethboxbutton.layer.borderWidth=2.0f;
    ninethboxbutton.tag=123;

    
    [ninethboxbutton addTarget:self action:@selector(ninethboxbuttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:ninethboxbutton];
    
    
}
-(void)playerchange{
    if (p==1){
        p=2;
    }
    else {
        p=1;
    }
}



-(void)firstboxButtonAction{
    
    if(!fifthboxbutton.backgroundColor){
       if(p==1){
    
     firstboxbutton.backgroundColor=[UIColor greenColor];
        [self playerchange];
           [self winningStatus];
    }
       else if (p==2){
        firstboxbutton.backgroundColor=[UIColor redColor];
    [self playerchange];
            [self winningStatus];
       }}
}
-(void)secondboxButtonAction{
    if(!fifthboxbutton.backgroundColor){
      if(p==1){
    
    secondboxbutton.backgroundColor=[UIColor greenColor];
          [self playerchange];
           [self winningStatus];
}

      else if (p==2){
          secondboxbutton.backgroundColor=[UIColor redColor];
          [self playerchange];
           [self winningStatus];
      }
    }}
    -(void)thirdboxButtonAction{
        
        if(!fifthboxbutton.backgroundColor){

        
       if(p==1){
            
            thirdboxbutton.backgroundColor=[UIColor greenColor];
            [self playerchange];
            [self winningStatus];
        }
        
        else if (p==2){
            thirdboxbutton.backgroundColor=[UIColor redColor];
            [self playerchange];
             [self winningStatus];
        }
        
        }}

-(void)forthboxbuttonAction{
    
    
    if(!fifthboxbutton.backgroundColor){

   if(p==1){
        
        forthboxbutton.backgroundColor=[UIColor greenColor];
        [self playerchange];
        [self winningStatus];
    }
    
    if (p==2){
        forthboxbutton.backgroundColor=[UIColor redColor];
        [self playerchange];
         [self winningStatus];
    }
    }}
-(void)fifthboxbuttonAction{
    
    if(!fifthboxbutton.backgroundColor){

    if(p==1){
        
        fifthboxbutton.backgroundColor=[UIColor greenColor];
        [self playerchange];
         [self winningStatus];
    }
    
    else if (p==2){
        fifthboxbutton.backgroundColor=[UIColor redColor];
        [self playerchange];
         [self winningStatus];
    }
    
    }}
-(void)sixthboxbuttonAction{
    
    if(!fifthboxbutton.backgroundColor){

    
    
    if(p==1){
        
        sixthboxbutton.backgroundColor=[UIColor greenColor];
        [self playerchange];
         [self winningStatus];
    }
    
    else if (p==2){
        sixthboxbutton.backgroundColor=[UIColor redColor];
        [self playerchange];
         [self winningStatus];
    }
    }
}

-(void)seventhboxbuttonAction{
    
    if(!fifthboxbutton.backgroundColor){

    
   if(p==1){
        
        seventhboxbutton.backgroundColor=[UIColor greenColor];
        [self playerchange];
        [self winningStatus];
    }
    
    else if (p==2){
        seventhboxbutton.backgroundColor=[UIColor redColor];
        [self playerchange];
         [self winningStatus];
    }
    
    
    }}

-(void)eightboxbuttonAction{
    
    if(!fifthboxbutton.backgroundColor){

  if(p==1){
        
        eightboxbutton.backgroundColor=[UIColor greenColor];
        [self playerchange];
       [self winningStatus];
    }
    
    else if (p==2){
        eightboxbutton.backgroundColor=[UIColor redColor];
        [self playerchange];
         [self winningStatus];
    }
    }
}
-(void)ninethboxbuttonAction{
    
    if(!fifthboxbutton.backgroundColor){

    
if(p==1){
        
        ninethboxbutton.backgroundColor=[UIColor greenColor];
        [self playerchange];
     [self winningStatus];
    }
    
    else if (p==2){
        ninethboxbutton.backgroundColor=[UIColor redColor];
        [self playerchange];
         [self winningStatus];
    }
    }}

-(void)winningStatus
{
    counter++;
    if (counter ==9)
    {
        
        
        
        
        
        
        if ([firstboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [secondboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [thirdboxbutton.backgroundColor isEqual:[UIColor greenColor]])
        {
            
            lineView = [[UIView alloc]initWithFrame:CGRectMake(40, 170, 240, 3)];
            lineView.backgroundColor = [UIColor redColor];
            [self addSubview:lineView];
            
            NSLog(@"x1 match");
            
        }
        else if([forthboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [sixthboxbutton.backgroundColor isEqual:[UIColor greenColor]])
        {
            lineView = [[UIView alloc]initWithFrame:CGRectMake(40, 250, 240, 3)];
            lineView2.backgroundColor = [UIColor redColor];
            [self addSubview:lineView];
            
            NSLog(@"x2 match");
        }
        else if([seventhboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [eightboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [ninethboxbutton.backgroundColor isEqual:[UIColor greenColor]])
        {
            lineView = [[UIView alloc]initWithFrame:CGRectMake(40, 350, 240, 3)];
            lineView.backgroundColor = [UIColor redColor];
            [self addSubview:lineView];
            
            NSLog(@"x3 match");
        }
        
        
        else if ([firstboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [forthboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [seventhboxbutton.backgroundColor isEqual:[UIColor greenColor]])
        {
             lineView = [[UIView alloc]initWithFrame:CGRectMake(70, 130, 3, 240)];
            lineView4.backgroundColor = [UIColor redColor];
            [self addSubview:lineView];
            
            
            NSLog(@"x4 match");
        }
        
        else if ([secondboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [eightboxbutton.backgroundColor isEqual:[UIColor greenColor]])
        {
            lineView = [[UIView alloc]initWithFrame:CGRectMake(160, 130, 3, 240)];
            lineView5.backgroundColor = [UIColor redColor];
            [self addSubview:lineView];
            
            NSLog(@"x5 match");
        }
        
        else if([thirdboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [sixthboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [ninethboxbutton.backgroundColor isEqual:[UIColor greenColor]])
        {
            lineView = [[UIView alloc]initWithFrame:CGRectMake(240, 130, 3, 240)];
            lineView6.backgroundColor = [UIColor redColor];
            [self addSubview:lineView6];
            NSLog(@"x6 match");
        }
        
        else  if ([firstboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [ninethboxbutton.backgroundColor isEqual:[UIColor greenColor]])
        {
            NSLog(@"x7 match");
            UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
            [pathHorizontal1 moveToPoint:CGPointMake(35, 125)];
            [pathHorizontal1 addLineToPoint:CGPointMake(290, 380)];
            CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
            shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
            shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
            shapeLayerHorizontal1.lineWidth = 3.0;
            shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
            [self.layer addSublayer:shapeLayerHorizontal1];
        }
        
        else if([thirdboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [seventhboxbutton.backgroundColor isEqual:[UIColor redColor]])
        {
            NSLog(@"x8 match");
            
            UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
            [pathHorizontal1 moveToPoint:CGPointMake(280, 120)];
            [pathHorizontal1 addLineToPoint:CGPointMake(20.0, 380)];
            CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
            shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
            shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
            shapeLayerHorizontal1.lineWidth = 3.0;
            shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
            [self.layer addSublayer:shapeLayerHorizontal1];
            
        }
        
  

#pragma winining status second condition



else if ([firstboxbutton.backgroundColor isEqual:[UIColor redColor  ]] && [secondboxbutton.backgroundColor isEqual:[UIColor redColor]] && [thirdboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
    
    lineView = [[UIView alloc]initWithFrame:CGRectMake(40, 170, 240, 3)];
    lineView.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    
    NSLog(@"x1 match");
    
}
else if([forthboxbutton.backgroundColor isEqual:[UIColor redColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor redColor]] && [sixthboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
    lineView = [[UIView alloc]initWithFrame:CGRectMake(40, 250, 240, 3)];
    lineView.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    
    NSLog(@"x2 match");
}
else if([seventhboxbutton.backgroundColor isEqual:[UIColor redColor]] && [eightboxbutton.backgroundColor isEqual:[UIColor redColor]] && [ninethboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
    lineView = [[UIView alloc]initWithFrame:CGRectMake(40, 350, 240, 3)];
    lineView.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    
    NSLog(@"x3 match");
}


else if ([firstboxbutton.backgroundColor isEqual:[UIColor redColor]] && [forthboxbutton.backgroundColor isEqual:[UIColor redColor]] && [seventhboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
     lineView = [[UIView alloc]initWithFrame:CGRectMake(70, 130, 3, 240)];
    lineView.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    
    
    NSLog(@"x4 match");
}

else if ([secondboxbutton.backgroundColor isEqual:[UIColor redColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor redColor]] && [eightboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
     lineView = [[UIView alloc]initWithFrame:CGRectMake(160, 130, 3, 240)];
    lineView.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    
    NSLog(@"x5 match");
}

else if([thirdboxbutton.backgroundColor isEqual:[UIColor redColor]] && [sixthboxbutton.backgroundColor isEqual:[UIColor redColor]] && [ninethboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
    lineView = [[UIView alloc]initWithFrame:CGRectMake(240, 130, 3, 240)];
    lineView.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    NSLog(@"x6 match");
}

else  if ([firstboxbutton.backgroundColor isEqual:[UIColor redColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor redColor]] && [ninethboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
    NSLog(@"x7 match");
    UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
    [pathHorizontal1 moveToPoint:CGPointMake(35, 125)];
    [pathHorizontal1 addLineToPoint:CGPointMake(290, 380)];
    CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
    shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
    shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
    shapeLayerHorizontal1.lineWidth = 3.0;
    shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
    [self.layer addSublayer:shapeLayerHorizontal1];
}

else if([thirdboxbutton.backgroundColor isEqual:[UIColor redColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor redColor]] && [seventhboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
    NSLog(@"x8 match");
    
    UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
    [pathHorizontal1 moveToPoint:CGPointMake(280, 120)];
    [pathHorizontal1 addLineToPoint:CGPointMake(20.0, 380)];
    CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
    shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
    shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
    shapeLayerHorizontal1.lineWidth = 3.0;
    shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
    [self.layer addSublayer:shapeLayerHorizontal1];
    
}


    else{
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Message" message:@"Match Draw" delegate:Nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
    }

}
if (counter>=5){
if ([firstboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [secondboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [thirdboxbutton.backgroundColor isEqual:[UIColor greenColor]])
{
    
    lineView = [[UIView alloc]initWithFrame:CGRectMake(40, 170, 240, 3)];
    lineView.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    
    NSLog(@"x1 match");
    
}
else if([forthboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [sixthboxbutton.backgroundColor isEqual:[UIColor greenColor]])
{
    lineView = [[UIView alloc]initWithFrame:CGRectMake(40, 250, 240, 3)];
    lineView2.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    
    NSLog(@"x2 match");
}
else if([seventhboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [eightboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [ninethboxbutton.backgroundColor isEqual:[UIColor greenColor]])
{
    lineView = [[UIView alloc]initWithFrame:CGRectMake(40, 350, 240, 3)];
    lineView.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    
    NSLog(@"x3 match");
}


else if ([firstboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [forthboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [seventhboxbutton.backgroundColor isEqual:[UIColor greenColor]])
{
    lineView = [[UIView alloc]initWithFrame:CGRectMake(70, 130, 3, 240)];
    lineView4.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    
    
    NSLog(@"x4 match");
}

else if ([secondboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [eightboxbutton.backgroundColor isEqual:[UIColor greenColor]])
{
    lineView = [[UIView alloc]initWithFrame:CGRectMake(160, 130, 3, 240)];
    lineView5.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    
    NSLog(@"x5 match");
}

else if([thirdboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [sixthboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [ninethboxbutton.backgroundColor isEqual:[UIColor greenColor]])
{
    lineView = [[UIView alloc]initWithFrame:CGRectMake(240, 130, 3, 240)];
    lineView6.backgroundColor = [UIColor redColor];
    [self addSubview:lineView6];
    NSLog(@"x6 match");
}

else  if ([firstboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [ninethboxbutton.backgroundColor isEqual:[UIColor greenColor]])
{
    NSLog(@"x7 match");
    UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
    [pathHorizontal1 moveToPoint:CGPointMake(35, 125)];
    [pathHorizontal1 addLineToPoint:CGPointMake(290, 380)];
    CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
    shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
    shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
    shapeLayerHorizontal1.lineWidth = 3.0;
    shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
    [self.layer addSublayer:shapeLayerHorizontal1];
}

else if([thirdboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor greenColor]] && [seventhboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
    NSLog(@"x8 match");
    
    UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
    [pathHorizontal1 moveToPoint:CGPointMake(280, 120)];
    [pathHorizontal1 addLineToPoint:CGPointMake(20.0, 380)];
    CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
    shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
    shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
    shapeLayerHorizontal1.lineWidth = 3.0;
    shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
    [self.layer addSublayer:shapeLayerHorizontal1];
    
}



#pragma winining status second condition



else if ([firstboxbutton.backgroundColor isEqual:[UIColor redColor  ]] && [secondboxbutton.backgroundColor isEqual:[UIColor redColor]] && [thirdboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
    
    lineView = [[UIView alloc]initWithFrame:CGRectMake(40, 170, 240, 3)];
    lineView.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    
    NSLog(@"x1 match");
    
}
else if([forthboxbutton.backgroundColor isEqual:[UIColor redColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor redColor]] && [sixthboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
    lineView = [[UIView alloc]initWithFrame:CGRectMake(40, 250, 240, 3)];
    lineView.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    
    NSLog(@"x2 match");
}
else if([seventhboxbutton.backgroundColor isEqual:[UIColor redColor]] && [eightboxbutton.backgroundColor isEqual:[UIColor redColor]] && [ninethboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
    lineView = [[UIView alloc]initWithFrame:CGRectMake(40, 350, 240, 3)];
    lineView.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    
    NSLog(@"x3 match");
}


else if ([firstboxbutton.backgroundColor isEqual:[UIColor redColor]] && [forthboxbutton.backgroundColor isEqual:[UIColor redColor]] && [seventhboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
    lineView = [[UIView alloc]initWithFrame:CGRectMake(70, 130, 3, 240)];
    lineView.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    
    
    NSLog(@"x4 match");
}

else if ([secondboxbutton.backgroundColor isEqual:[UIColor redColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor redColor]] && [eightboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
    lineView = [[UIView alloc]initWithFrame:CGRectMake(160, 130, 3, 240)];
    lineView.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    
    NSLog(@"x5 match");
}

else if([thirdboxbutton.backgroundColor isEqual:[UIColor redColor]] && [sixthboxbutton.backgroundColor isEqual:[UIColor redColor]] && [ninethboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
    lineView = [[UIView alloc]initWithFrame:CGRectMake(240, 130, 3, 240)];
    lineView.backgroundColor = [UIColor redColor];
    [self addSubview:lineView];
    NSLog(@"x6 match");
}

else  if ([firstboxbutton.backgroundColor isEqual:[UIColor redColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor redColor]] && [ninethboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
    NSLog(@"x7 match");
    UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
    [pathHorizontal1 moveToPoint:CGPointMake(35, 125)];
    [pathHorizontal1 addLineToPoint:CGPointMake(290, 380)];
    CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
    shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
    shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
    shapeLayerHorizontal1.lineWidth = 3.0;
    shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
    [self.layer addSublayer:shapeLayerHorizontal1];
}

else if([thirdboxbutton.backgroundColor isEqual:[UIColor redColor]] && [fifthboxbutton.backgroundColor isEqual:[UIColor redColor]] && [seventhboxbutton.backgroundColor isEqual:[UIColor redColor]])
{
    NSLog(@"x8 match");
    
    UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
    [pathHorizontal1 moveToPoint:CGPointMake(280, 120)];
    [pathHorizontal1 addLineToPoint:CGPointMake(20.0, 380)];
    CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
    shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
    shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
    shapeLayerHorizontal1.lineWidth = 3.0;
    shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
    [self.layer addSublayer:shapeLayerHorizontal1];
    
}

}}


@end


