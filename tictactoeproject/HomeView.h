//
//  HomeView.h
//  tictactoeproject
//
//  Created by Click Labs 107 on 10/13/15.
//  Copyright (c) 2015 tashanpunjabi. All rights reserved.



#import <UIKit/UIKit.h>

@protocol ButtonProtocolName <NSObject>

-(void) buttonWasPressed;
-(void)scorebuttonPresses;



@end
@interface HomeView : UIView




 
@property (nonatomic, assign) id <ButtonProtocolName> delegate;
@property (nonatomic,strong)UILabel*firstPlayerScore;
@property (nonatomic,strong)UILabel*secondPlayerScore;
-(void)buttons;
-(void)startButtonAction;
-(void)scoreButtonAction;
-(void)settingButtonAction;
-(void)avAudioAction;



@end
