//
//  PlayersView.h
//  tictactoeproject
//
//  Created by Click Labs 107 on 10/13/15.
//  Copyright (c) 2015 tashanpunjabi. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol startbuttonClassDelegate 


-(void)startbuttonPresses;
-(void)backbuttonpressed;
//-(void)textFieldscopy:(NSString*)string;
//-(void)textFieldscopy1:(NSString*)string1;


@end
@interface PlayersView : UIView

@property (nonatomic, assign)id <startbuttonClassDelegate> delegateFirst;
@property (nonatomic,strong)NSString *string;

@property (nonatomic,strong)NSString *string1;

-(void)textFields;
-(void)Button;
-(void)backButton;
@end
