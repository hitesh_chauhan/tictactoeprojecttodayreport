//
//  ViewController.m
//  tictactoeproject
//
//  Created by Click Labs 107 on 10/13/15.
//  Copyright (c) 2015 tashanpunjabi. All rights reserved.
//

#import "ViewController.h"
#import "HomeView.h"
#import "PlayersView.h"
#import "gamePlay.h"

@interface ViewController ()<ButtonProtocolName,startbuttonClassDelegate,gamePlayClassDelegate>
@end


@implementation ViewController
{
    
    HomeView*homeViewObject;
    PlayersView*playerViewObject;
    gamePlay*userGamePlay;
  
   
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self homeViews];
    homeViewObject.delegate=self;
    playerViewObject.delegateFirst=self;
    
}

-(void)homeViews{

    homeViewObject=[[HomeView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    homeViewObject.backgroundColor=[UIColor grayColor];
    [homeViewObject buttons];
    //homeViewObject.scorefirst.text=firstPlayername.text ;
    [homeViewObject avAudioAction];
    
    
    
    
    [self.view addSubview:homeViewObject];
    
}

-(void)buttonWasPressed{
    
   
    playerViewObject=[[PlayersView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        playerViewObject.backgroundColor=[UIColor grayColor];
        [playerViewObject textFields];
        [playerViewObject backButton];
        [playerViewObject Button];
        playerViewObject.delegateFirst=self;
    
    [self.view addSubview:playerViewObject];
    
  
}

-(void)startbuttonPresses{
   
    
    userGamePlay=[[gamePlay alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    userGamePlay.backgroundColor=[UIColor grayColor];
    [userGamePlay playersnames];
    [userGamePlay buttons];
    
    userGamePlay .firstPlayer.text =playerViewObject.string;
userGamePlay.secondPlayer.text=playerViewObject.string1;
  
   userGamePlay.delegateFirst=self;
 
    [self.view addSubview:userGamePlay];
    
    
    NSLog(@"test %@ %@",_secondPlayername,_firstPlayername);
    
        
        
    
}
-(void)backbuttonpressed{
    
    homeViewObject=[[HomeView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    homeViewObject.backgroundColor=[UIColor grayColor];
    [homeViewObject buttons];
    //homeViewObject.scorefirst.text=firstPlayername.text ;
    [homeViewObject avAudioAction];
    
    homeViewObject.delegate=self;

    [self.view addSubview:homeViewObject];

}

-(void)gamePlayBack{
    homeViewObject=[[HomeView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    homeViewObject.backgroundColor=[UIColor grayColor];
    [homeViewObject buttons];
    //homeViewObject.scorefirst.text=firstPlayername.text ;
    [homeViewObject avAudioAction];
     
    playerViewObject.delegateFirst=self;
    homeViewObject.delegate=self;
    
    [self.view addSubview:homeViewObject];
    
    
    
}

-(void)scorebuttonPresses{
    homeViewObject.firstPlayerScore.text =playerViewObject.string;
    homeViewObject.secondPlayerScore.text=playerViewObject.string1;
    
}

-(void) resetbuttonaction{
    
    userGamePlay=[[gamePlay alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    userGamePlay.backgroundColor=[UIColor grayColor];
    [userGamePlay playersnames];
    [userGamePlay buttons];
    userGamePlay.delegateFirst=self;
    
    [self.view addSubview:userGamePlay];
    
    
    
}
//-(void)scorebuttonPresses{
//    
//    
//    homeViewObject=[[HomeView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    homeViewObject.backgroundColor=[UIColor grayColor];
//    homeViewObject.
//    
//}


//-(void)textFieldscopy:(NSString*)string{
//   
//    userGamePlay.firstPlayer.text=string;
//    
//    if (userGamePlay) {
//        [userGamePlay removeFromSuperview];
//        userGamePlay.delegateFirst=nil;
//        userGamePlay = nil;
//        
//        
//    }
//    NSLog(@"name is %@",_firstPlayername);
//    
//    
//}
//-(void)textFieldscopy1:(NSString*)string1{
//    
//  
//    userGamePlay.secondPlayer.text=string1;
//    if (userGamePlay) {
//        [userGamePlay removeFromSuperview];
//        userGamePlay.delegateFirst=nil;
//        userGamePlay = nil;
//        
//        NSLog(@"name is %@",_secondPlayername);
//    }
//}

//-(void)textfieldsview{
//    
//    textFieldsView=[[PlayersView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    
//    
//    
//    textFieldsView.backgroundColor=[UIColor grayColor];
//    [textFieldsView textFields];
//    [textFieldsView Button];
//    [textFieldsView backButton];
//    [self.view addSubview:textFieldsView];
//    
//}

//-(void)gameViews{
//    
//    userGamePlay=[[gamePlay alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    
//    userGamePlay.backgroundColor=[UIColor grayColor];
//    
//    [userGamePlay playersnames];
//    //userGamePlay.firstPlayer=firstPlayerName.text;
//    
//    [self.view addSubview:userGamePlay];
//    
//}
//
//
//- (void)PlayerViewClassDelegate:(UITextField *)firstPlayerName{
//    firstPlayername.text=firstPlayerName.text;
//}
//
////- (void)PlayerViewClassDelegate:(UITextField *)secondPlayerName{
////    secondPlayername.text=secondPlayerName;
////    
////}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
