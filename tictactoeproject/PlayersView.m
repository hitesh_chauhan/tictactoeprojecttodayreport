//
//  PlayersView.m
//  tictactoeproject
//
//  Created by Click Labs 107 on 10/13/15.
//  Copyright (c) 2015 tashanpunjabi. All rights reserved.
//

#import "PlayersView.h"
#import "HomeView.h"
#import "gamePlay.h"
#import "ViewController.h"

@implementation PlayersView

{
     UITextField*firstPlayerName;
     UITextField*secondPlayerName;
    gamePlay*gamePlayView;
    UIButton*startButton1;
}

-(void)textFields{
    
    firstPlayerName=[[UITextField alloc]initWithFrame:CGRectMake(10, 80, 160, 60)];
    firstPlayerName.keyboardType=UIKeyboardTypeAlphabet;
    firstPlayerName.placeholder=@" First Player";
    firstPlayerName.layer.borderWidth=2.0f;
    [[firstPlayerName layer] setBorderColor:[UIColor redColor].CGColor];
//     //firstName=[[NSString alloc]init];
   _string=[firstPlayerName.text copy];
    [self addSubview:firstPlayerName];
 
    
    
    secondPlayerName=[[UITextField alloc]initWithFrame:CGRectMake(10, 200, 160, 60)];
    secondPlayerName.keyboardType=UIKeyboardTypeAlphabet;
    secondPlayerName.placeholder=@" Second Player";

    secondPlayerName.layer.borderWidth=2.0f;
    [[secondPlayerName layer] setBorderColor:[UIColor redColor].CGColor];
    //secondName=[[NSString alloc]init];
  _string1=[firstPlayerName.text copy];

    [self addSubview:secondPlayerName];
    
    UIImageView*bottomImage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"WoodTacToe.jpg"]];
    bottomImage.frame=CGRectMake(0, 320, 320, 280);
    
    [self addSubview:bottomImage];
    
    
}
-(void)Button{
    
    startButton1=[[UIButton alloc]initWithFrame:CGRectMake(190, 270, 120, 40)];
    startButton1.backgroundColor=[UIColor yellowColor];
    [[startButton1 layer] setBorderColor:[UIColor redColor].CGColor];
    startButton1.layer.borderWidth=2.0f;
    startButton1.tag=105;
    [startButton1 setTitle:@"START" forState:UIControlStateNormal];
    [startButton1 setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [startButton1 addTarget:self action:@selector(startButton1Action) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:startButton1];
    
    
    
}

-(void)backButton{
    
    
    UIButton*backButton=[[UIButton alloc]initWithFrame:CGRectMake(190, 20, 120, 40)];
    backButton.backgroundColor=[UIColor yellowColor];
    [[backButton layer] setBorderColor:[UIColor redColor].CGColor];
    backButton.layer.borderWidth=2.0f;
    [backButton setTitle:@"BACK" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:backButton];
    
    
}

-(void)startButton1Action{
    if(![firstPlayerName.text isEqual:@""]||![secondPlayerName.text isEqual:@""])
    {
        _string=firstPlayerName.text;
        _string1=secondPlayerName.text;
//        gamePlayView.firstPlayer.text=_string;
//        gamePlayView.secondPlayer.text=_string1;

        [self.delegateFirst startbuttonPresses];
        NSLog(@"names ar %@ %@",_string,_string1);

    [self endEditing:YES];
    }
    else if ([firstPlayerName.text isEqual:@""]||[secondPlayerName.text isEqual:@""])
    {
        UIAlertView*alertView=[[UIAlertView alloc]initWithTitle:@"MESSAGE" message:@"Please add player names" delegate:self cancelButtonTitle:@"BACK" otherButtonTitles:Nil, nil];
        [alertView show];
    }
    
 
    
}




-(void)backButtonAction{
    
    [self.delegateFirst backbuttonpressed];
}
    
    

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    //[self endEditing:YES];
    
    
}
//-(void)textFieldscopy:(NSString*)string{
//  
//    string=firstPlayerName.text;
//    [self.delegateFirst startbuttonPresses];
//    
//}
//-(void)textFieldscopy1:(NSString*)string1{
// 
//
//
//    string1=secondPlayerName.text;
//    [self.delegateFirst startbuttonPresses];
//    
//}

@end
