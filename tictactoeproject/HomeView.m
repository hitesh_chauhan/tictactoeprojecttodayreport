//
//  HomeView.m
//  tictactoeproject
//
//  Created by Click Labs 107 on 10/13/15.
//  Copyright (c) 2015 tashanpunjabi. All rights reserved.
//

#import "HomeView.h"
#import "ViewController.h"
//#import "PlayersView.h"
#import <AVFoundation/AVFoundation.h>

@implementation HomeView
{
   // PlayersView*playerViewObject;
    
        UIView*settingView;
        AVAudioPlayer *audioObject;
        UISwitch *mySwitch;
       UILabel*settingLabel;
      UILabel*soundLabel;
      UIView*scoreView;
    ViewController*controller;
    UILabel*scoresecond;
    HomeView*firstHomeView;
    UIButton*startButton;
    UILabel*scorefirst;
}





-(void)buttons {
    
  
    
    
    startButton=[[UIButton alloc]initWithFrame:CGRectMake(60, 90, 120, 40)];
    startButton.backgroundColor=[UIColor yellowColor];
    [[startButton layer] setBorderColor:[UIColor redColor].CGColor];
    startButton.layer.borderWidth=1.0f;
    startButton.tag=101;
    [startButton setTitle:@"START" forState:UIControlStateNormal];
    [startButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [startButton addTarget:self action:@selector(startButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:startButton];
    
    
    UIButton*settingButton=[[UIButton alloc]initWithFrame:CGRectMake(60, 160, 120, 40)];
    settingButton.backgroundColor=[UIColor yellowColor];
    [[settingButton layer] setBorderColor:[UIColor redColor].CGColor];
    settingButton.layer.borderWidth=1.0f;
    [settingButton setTitle:@"SETTING" forState:UIControlStateNormal];
    [settingButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [settingButton addTarget:self action:@selector(settingButtonAction) forControlEvents:UIControlEventTouchUpInside];

    [self addSubview:settingButton];
    
    
    
    
    UIButton*scoreButton=[[UIButton alloc]initWithFrame:CGRectMake(60, 230, 120, 40)];
    scoreButton.backgroundColor=[UIColor yellowColor];
    [[scoreButton layer] setBorderColor:[UIColor redColor].CGColor];
    scoreButton.layer.borderWidth=1.0f;
    [scoreButton setTitle:@"SCORE" forState:UIControlStateNormal];
    [scoreButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    [scoreButton addTarget:self action:@selector(scoreButtonAction) forControlEvents:UIControlEventTouchUpInside];


    [self addSubview:scoreButton];
    
    
    UIImageView*bottomImage=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"WoodTacToe.jpg"]];
    bottomImage.frame=CGRectMake(0, 280, 320, 280);
    bottomImage.userInteractionEnabled=YES;
    [self addSubview:bottomImage];
    
    
    
    
}

-(void)startButtonAction:(id)sender{
    if (startButton.tag==101){
  [self.delegate buttonWasPressed];
   
    }
}


-(void)scoreButtonAction{
    
    
    scoreView=[[UIView alloc]initWithFrame:CGRectMake(0, 280,320, 280)];
    scoreView.backgroundColor=[UIColor lightGrayColor];
    scoreView.userInteractionEnabled=YES;
    [self addSubview:scoreView];
    
    _firstPlayerScore=[[UILabel alloc]initWithFrame:CGRectMake(10  ,300, 200, 40)];
    // settingLabel.backgroundColor=[UIColor ];
    _firstPlayerScore.text=@"FIRST PLAYER SCORE" ;
    _firstPlayerScore.textColor=[UIColor brownColor];
    _firstPlayerScore.layer.borderWidth=1.5f;

    [self addSubview:_firstPlayerScore];
    
    _secondPlayerScore=[[UILabel alloc]initWithFrame:CGRectMake(10  ,360, 200, 40)];
    //soundLabel.backgroundColor=[UIColor yellowColor];
    _secondPlayerScore.textColor=[UIColor brownColor];
    _secondPlayerScore.layer.borderWidth=1.5f;
    _secondPlayerScore.text=@"SECOND PLAYER SCORE" ;
    [self addSubview:_secondPlayerScore];
    
    scorefirst=[[UILabel alloc]initWithFrame:CGRectMake(250  ,300, 60, 40)];
   

    scorefirst.textColor=[UIColor brownColor];
    scorefirst.layer.borderWidth=1.5f;
    
    [self addSubview:scorefirst];
    
    scoresecond=[[UILabel alloc]initWithFrame:CGRectMake(250  ,360, 60, 40)];
  
 
    scoresecond.textColor=[UIColor brownColor];
    scoresecond.layer.borderWidth=1.5f;
    
    [self.delegate scorebuttonPresses];
    
    [self addSubview:scoresecond];
    
    
    
    
    
    
}
-(void)settingButtonAction{
    
    
    settingView=[[UIView alloc]initWithFrame:CGRectMake(0, 280,320, 280)];
    settingView.backgroundColor=[UIColor lightGrayColor];
    settingView.userInteractionEnabled=YES;
    [self addSubview:settingView];
    
    
    settingLabel=[[UILabel alloc]initWithFrame:CGRectMake(100  ,300, 100, 40)];
   // settingLabel.backgroundColor=[UIColor ];
    settingLabel.text=@"SETTINGS " ;
    settingLabel.textColor=[UIColor brownColor];
    settingLabel.layer.borderWidth=1.5f;

    settingLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:settingLabel];
    
    soundLabel=[[UILabel alloc]initWithFrame:CGRectMake(10  ,360, 100, 40)];
    //soundLabel.backgroundColor=[UIColor yellowColor];
    soundLabel.textColor=[UIColor brownColor];
    soundLabel.layer.borderWidth=1.5f;
    
    soundLabel.text=@"SOUND ";
    [self addSubview:soundLabel];
    
    mySwitch=[[UISwitch alloc]init];
    mySwitch.frame = CGRectMake(200, 360, 250.0f, 25.0f);
    
   
    [mySwitch addTarget:self action:@selector(switchIsChanged:)forControlEvents:UIControlEventValueChanged];
    
    [self addSubview:mySwitch];
    
    
    
}

- (void) switchIsChanged:(UISwitch *)paramSender{
    if(paramSender.on==TRUE){
    [audioObject play];
    }
    
    else  {
        [audioObject stop];
    }
}




-(void)avAudioAction{
    NSURL *urlObject = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"tic tac song" ofType:@"mp3"]];
    
    audioObject = [[AVAudioPlayer alloc] initWithContentsOfURL:urlObject error:nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    audioObject.volume = 1;
    //[audioObject play];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [settingView removeFromSuperview];
    [soundLabel removeFromSuperview];
    [settingLabel removeFromSuperview];
    [mySwitch removeFromSuperview];
    [scoreView removeFromSuperview];
    [_firstPlayerScore removeFromSuperview];
    [_secondPlayerScore removeFromSuperview];
    
    [scorefirst removeFromSuperview];
    [scoresecond removeFromSuperview];
    
}




@end
