//
//  gamePlay.h
//  tictactoeproject
//
//  Created by Click Labs108 on 10/15/15.
//  Copyright © 2015 tashanpunjabi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol gamePlayClassDelegate


-(void)gamePlayBack;
-(void)resetbuttonaction;
@end

@interface gamePlay : UIView

@property (nonatomic, assign)id <gamePlayClassDelegate> delegateFirst;

-(void) playersnames;
-(void)buttons;
@property(nonatomic,strong) UILabel*firstPlayer;
  @property(nonatomic,strong)  UILabel*secondPlayer;
@end
